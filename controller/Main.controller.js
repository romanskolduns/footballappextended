sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "FootballAppExtended/model/models",
], function (Controller, models) {
    "use strict";
    return sap.ui.controller("FootballAppExtended.controller.Main", {

        extPointCheckFileExtension:function (fileExt) {
			return (fileExt === 'xml');
        },
        extPointConvertToJson:function (file,fileExt) {
			if (fileExt === 'xml') {
                var xmlDoc = $.parseXML(file);
                return  this.xmlToJson(xmlDoc);
			} else return false;

        },

        processFootballData: function() {
			if (!this.global.oFootballData.length) {
				return;
			}
		
            this.models.processFootballData();

			if (this.getView().getModel()) {
				this.getView().getModel().refresh(true);
			}
		},
        xmlToJson: function(xml) {
            // Create the return object
            var obj = {};
        
            if (xml.nodeType == 1) { // element
                // do attributes
                if (xml.attributes.length > 0) {
                obj["@attributes"] = {};
                    for (var j = 0; j < xml.attributes.length; j++) {
                        var attribute = xml.attributes.item(j);
                        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                    }
                }
            } else if (xml.nodeType == 3) { // text
                obj = xml.nodeValue;
            }
        
            // do children
            if (xml.hasChildNodes()) {
                for(var i = 0; i < xml.childNodes.length; i++) {
                    var item = xml.childNodes.item(i);
                    var nodeName = item.nodeName;
                    if (typeof(obj[nodeName]) == "undefined") {
                        obj[nodeName] = this.xmlToJson(item);
                    } else {
                        if (typeof(obj[nodeName].push) == "undefined") {
                            var old = obj[nodeName];
                            obj[nodeName] = [];
                            obj[nodeName].push(old);
                        }
                        obj[nodeName].push(this.xmlToJson(item));
                    }
                }
            }
            return obj;
        }
    });
});
