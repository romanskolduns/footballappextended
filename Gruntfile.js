/*
 * Copyright (C) 2015 . All rights reserved
 */
/*eslint-env node*/
/*global module:false*/


// Actual Grunt script
module.exports = function(grunt) {
  grunt.initConfig({
    dir: {
      src: 'src',
      dist: 'dist'
    },

    connect: {
      server:{
        options: {
          port: '8082',
          hostname: 'localhost',
          keepalive: true,
          base: '.'
        }

      }
    },

    clean: {
      dist: '<%= dir.dist %>/',
      options: {
        force: true
      }
    },

    copy: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= dir.src %>',
          src: [
            '**',
            '!test/**'
          ],
          dest: '<%= dir.dist %>'
        }]
      }
    },

    eslint: {
      options: {
        quiet: true,
        config: '.eslintrc.json'
      },

      all: [
        '<%= dir.src %>'
      ],
      src: [
        '<%= dir.src %>'
      ]
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-connect-proxy');

  // Server task
  grunt.registerTask('serve', [
    'connect'
  ]);

  // Linting task
  grunt.registerTask('lint', [
    'eslint:all'
  ]);

  // Build task
  grunt.registerTask('build', [
    'eslint:src',
    'clean',
    'openui5_preload',
    'copy'
  ]);

  // Default task
  grunt.registerTask('default', [
    'lint:all'
  ]);
};
