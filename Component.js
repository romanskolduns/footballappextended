sap.ui.define([
    "FootballApp/Component",
    "sap/ui/core/UIComponent"
], function(Component, UIComponent) {
    "use strict";

    return Component.extend("FootballAppExtended.Component", {
        metadata: {
            version: "1.0",
            config: {},
            customizing: {
                "sap.ui.viewReplacements": {
                    "FootballApp.view.Game": {
                        viewName: "FootballAppExtended.view.Game",
                        type: "XML"
                    },
                    "FootballApp.view.Main": {
                        viewName: "FootballAppExtended.view.Main",
                        type: "XML"
                    }
                },
                "sap.ui.controllerExtensions": {
					"FootballApp.controller.Main": {
						"controllerName": "FootballAppExtended.controller.Main"
					}
				}
            }
        },
        init: function() {
            UIComponent.prototype.init.apply(this, arguments);
            this.getRouter().getTargets().addTarget("Substitutions",{controlAggregation: "detailPages", viewName:"Substitutions",viewPath:"FootballAppExtended.view",rootView:this.getAggregation("rootControl").getId()});
            this.getRouter().addRoute({name:"Substitutions",pattern:"Substitutions",target: [
                "Main",
                "Substitutions"
            ] });

            
            
            this.getRouter().initialize();
        }
    });
});
