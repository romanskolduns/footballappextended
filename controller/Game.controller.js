jQuery.sap.require("jquery.sap.storage");

sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "FootballApp/model/models",
    "FootballApp/util/Global",
    "sap/ui/core/routing/History"
], function(Controller, models, Global, History) {
    "use strict";

    return Controller.extend("FootballAppExtended.controller.Game", {
        global: Global,
        models: models,
        onInit: function() {
            var globalModel = sap.ui.getCore().getModel("globalModel");
            this.getView().setModel(globalModel);
            globalModel.refresh(true);
        }

    });
});